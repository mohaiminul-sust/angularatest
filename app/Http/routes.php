<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('index');
});

//api routes

Route::group(['prefix' => 'api/v1'], function(){

    Route::group(['prefix' => 'employees'], function(){

        Route::get('/{id?}', 'Employees@index');
        Route::post('/', 'Employees@store');
        Route::post('/{id}', 'Employees@update');
        Route::delete('/{id}', 'Employees@destroy');
    });
});
